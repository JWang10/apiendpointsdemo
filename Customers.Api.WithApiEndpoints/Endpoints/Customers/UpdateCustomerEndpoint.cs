using Ardalis.ApiEndpoints;
using Customers.Api.WithApiEndpoints.Attributes;
using Customers.Api.WithApiEndpoints.Models;
using Customers.Api.WithApiEndpoints.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Customers.Api.WithApiEndpoints.Endpoints.Customers;

public class UpdateCustomerRequest
{
    [FromRoute(Name = "id")] public Guid Id { get; init; }
    [FromBody] public Customer UpdateCustomer { get; set; } = default!;
}

public class UpdateCustomerEndpoint : EndpointBaseAsync
    .WithRequest<UpdateCustomerRequest>
    .WithActionResult<Customer>
{
    private readonly ICustomersService _customersService;

    public UpdateCustomerEndpoint(ICustomersService customersService)
    {
        _customersService = customersService;
    }

    [HttpPut("customers/{id:guid}")]
    [SwaggerOperation(
        Summary = "Update a Customer",
        Description = "Update a Customer",
        OperationId = "customer.Update",
        Tags = new []{"CustomerEndpoint"})
    ]
    public override async Task<ActionResult<Customer>> HandleAsync(
        // [FromMultiSource] UpdateCustomerRequest request
        [FromRoute] UpdateCustomerRequest request
        , CancellationToken cancellationToken = default)
    {
        var customer = await _customersService.GetByIdAsync(request.Id);
        if (customer is null)
        {
            return NotFound();
        }

        customer.FullName = request.UpdateCustomer.FullName;
        await _customersService.UpdateAsync(customer);
        return Ok(customer);
    }
}