using Ardalis.ApiEndpoints;
using Customers.Api.WithApiEndpoints.Models;
using Customers.Api.WithApiEndpoints.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Customers.Api.WithApiEndpoints.Endpoints.Customers;

public class DeleteCustomerEndpoint : EndpointBaseAsync
    .WithRequest<Guid>
    .WithActionResult<Customer>
{
    private readonly ICustomersService _customersService;

    public DeleteCustomerEndpoint(ICustomersService customersService)
    {
        _customersService = customersService;
    }

    [HttpDelete("customers/{id:guid}")]
    [SwaggerOperation(
        Summary = "Delete a Customer",
        Description = "Delete a Customer",
        OperationId = "customer.Delete",
        Tags = new []{"CustomerEndpoint"})
    ]
    public override async Task<ActionResult<Customer>> HandleAsync(
        [FromRoute] Guid id,
        CancellationToken cancellationToken = default)
    {
        var deleted = await _customersService.DeleteAsync(id);
        Console.WriteLine(deleted);
        if (!deleted)
        {
            return NotFound();
        }
        return Ok();
    }
}