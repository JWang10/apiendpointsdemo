using Ardalis.ApiEndpoints;
using Customers.Api.WithApiEndpoints.Models;
using Customers.Api.WithApiEndpoints.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Customers.Api.WithApiEndpoints.Endpoints.Customers;

public class GetAllCustomersEndpoint : EndpointBaseAsync
    .WithoutRequest
    .WithActionResult<List<Customer>>
{
    private readonly ICustomersService _customersService;
    
    public GetAllCustomersEndpoint(ICustomersService customersService)
    {
        _customersService = customersService;
    }

    [HttpGet("customers")]
    [SwaggerOperation(
        Summary = "Get All Customers",
        Description = "Get All Customers",
        OperationId = "customer.GetAll",
        Tags = new []{"CustomerEndpoint"})
    ]
    public override async Task<ActionResult<List<Customer>>> HandleAsync(
            CancellationToken cancellationToken = default)
    {
        var customers = await _customersService.GetAllAsync();
        return Ok(customers);
    }
}