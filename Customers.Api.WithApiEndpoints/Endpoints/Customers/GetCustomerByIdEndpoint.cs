using Ardalis.ApiEndpoints;
using Customers.Api.WithApiEndpoints.Models;
using Customers.Api.WithApiEndpoints.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Customers.Api.WithApiEndpoints.Endpoints.Customers;

public class GetCustomerByIdEndpoint : EndpointBaseAsync
    .WithRequest<Guid>
    .WithActionResult<Customer>
{
    private readonly ICustomersService _customersService;

    public GetCustomerByIdEndpoint(ICustomersService customersService)
    {
        _customersService = customersService;
    }

    [HttpGet("customers/{id:guid}")]
    [SwaggerOperation(
        Summary = "Get a Customer by Id",
        Description = "Get a Customer by Id",
        OperationId = "customer.GetCustomer",
        Tags = new []{"CustomerEndpoint"})
    ]
    public override async Task<ActionResult<Customer>> HandleAsync(
        [FromRoute] Guid id,
        CancellationToken cancellationToken = default)
    {
        var customer = await _customersService.GetByIdAsync(id);
        if (customer is null)
        {
            return NotFound();
        }
        return Ok(customer);
    }
}