using Ardalis.ApiEndpoints;
using Customers.Api.WithApiEndpoints.Models;
using Customers.Api.WithApiEndpoints.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Customers.Api.WithApiEndpoints.Endpoints.Customers;

public class CreateCustomerEndpoint : EndpointBaseAsync
    .WithRequest<Customer>
    .WithActionResult<Customer>
{
    private readonly ICustomersService _customersService;

    public CreateCustomerEndpoint(ICustomersService customersService)
    {
        _customersService = customersService;
    }

    [HttpPost("customers")]
    [SwaggerOperation(
        Summary = "Create a Customer",
        Description = "Create a Customer",
        OperationId = "customer.Create",
        Tags = new []{"CustomerEndpoint"})
    ]
    public override async Task<ActionResult<Customer>> HandleAsync(
        [FromBody] Customer customer,
        CancellationToken cancellationToken = default)
    {
        var created = await _customersService.CreateAsync(customer);
        if (!created)
        {
            return BadRequest();
        }
        return Created($"customer/{customer.Id}",customer);
    }
}