using Customers.Api.WithApiEndpoints.Models;

namespace Customers.Api.WithApiEndpoints.Services;

public class CustomersService : ICustomersService
{
    private readonly Dictionary<Guid, Customer> _customers = new();

    public Task<bool> CreateAsync(Customer? customer)
    {
        if (customer is null)
        {
            return Task.FromResult(false);
        }

        _customers[customer.Id] = customer;
        return Task.FromResult(true);
    }

    public Task<Customer?> GetByIdAsync(Guid id)
    {
        return  Task.FromResult(_customers.GetValueOrDefault(id));
    }

    public Task<List<Customer>> GetAllAsync()
    {
        foreach (var item in _customers)
        {
            Console.WriteLine($"{item.Key} {item.Value}");
        }

        return Task.FromResult(_customers.Values.ToList());
    }

    public Task<bool> UpdateAsync(Customer customer)
    {
        var existingCustomer = GetByIdAsync(customer.Id);
        if (existingCustomer.Result is null)
        {
            return Task.FromResult<bool>(false);
        }

        _customers[customer.Id] = customer;
        return Task.FromResult(true);
    }

    // public bool Delete(Customer customer)
    public Task<bool> DeleteAsync(Guid id)
    {
        var existingCustomer = GetByIdAsync(id);
        if (existingCustomer.Result is null)
        {
            return Task.FromResult<bool>(false);
        }

        _customers.Remove(id);
        return Task.FromResult<bool>(true);
    }
}