using Customers.Api.WithApiEndpoints.Models;

namespace Customers.Api.WithApiEndpoints.Services;

public interface ICustomersService
{
    Task<bool> CreateAsync(Customer? customer);
    Task<Customer?> GetByIdAsync(Guid id);
    Task<List<Customer>> GetAllAsync();
    Task<bool> UpdateAsync(Customer customer);
    Task<bool> DeleteAsync(Guid id);
}