using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Customers.Api.WithApiEndpoints.Attributes;

public class FromMultiSourceAttribute : Attribute, IBindingSourceMetadata
{
    public BindingSource? BindingSource { get; }
}