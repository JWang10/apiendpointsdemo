using Customers.Api.WithControllers.Models;
using Customers.Api.WithControllers.Services;
using Microsoft.AspNetCore.Mvc;

namespace Customers.Api.WithControllers.Controllers;

[ApiController]
[Route("customers")]
public class CustomersController : ControllerBase
{
    private readonly ICustomersService _customersService;

    public CustomersController(ICustomersService customersService)
    {
        _customersService = customersService;
    }
    
    [HttpGet]
    public IActionResult GetAll()
    {
        var customers = _customersService.GetAll();
        return Ok(customers);
    }

    [HttpGet("{id:guid}")]
    public IActionResult GetById([FromRoute] Guid id)
    {
        var customer = _customersService.GetById(id);
        if (customer is null)
        {
            return NotFound();
        }
        return Ok(customer);
    }
    [HttpPost]
    public IActionResult Create([FromBody] Customer customer)
    {
        var created = _customersService.Create(customer);
        if (!created)
        {
            return BadRequest();
        }
        return CreatedAtAction(nameof(GetById),new { id = customer.Id },customer);
    }
    [HttpPut("{id:guid}")]
    public IActionResult Update([FromRoute] Guid id, [FromBody] Customer updateCustomer)
    {
        var customer = _customersService.GetById(id);
        if (customer is null)
        {
            return NotFound();
        }
        _customersService.Update(updateCustomer);
        return Ok(updateCustomer);
    }

    [HttpDelete("{id:guid}")]
    public IActionResult Delete([FromRoute] Guid id)
    {
        var deleted = _customersService.Delete(id);
        if (!deleted)
        {
            return NotFound();
        }
        return Ok();
    }
}