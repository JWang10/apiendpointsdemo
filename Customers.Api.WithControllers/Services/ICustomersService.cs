using Customers.Api.WithControllers.Models;

namespace Customers.Api.WithControllers.Services;

public interface ICustomersService
{
    bool Create(Customer? customer);
    Customer? GetById(Guid id);
    List<Customer> GetAll();
    bool Update(Customer customer);
    bool Delete(Guid id);
}