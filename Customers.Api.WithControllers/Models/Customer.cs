using Microsoft.AspNetCore.Mvc;

namespace Customers.Api.WithControllers.Models;

public class Customer
{
    public Guid Id { get; init; } = Guid.NewGuid();
    public string? FullName { get; set; }
}