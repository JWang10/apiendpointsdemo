# ApiEndpoints Demo

> Most REST APIs have groups of endpoints for a given resource.
> **In Controller-based projects you would have a controller per resource.
> When using API Endpoints you can simply create a folder per resource,**
> just as you would use folders to group related pages in Razor Pages.

practice for the concept of Api Endpoint


ref:
- [Clean up your .NET Controllers with API Endpoints by Nick Chapsas](https://youtu.be/SDu0MA6TmuM)
- [ASP.NET Core API Endpoints](https://github.com/ardalis/ApiEndpoints)